$(document).ready(function() {
	$("#login").on("submit", function(event) {
		event.preventDefault();
		var username = $("#username").val()
		var password = $("#password").val()
		if (username !== "" && password !== "") {
			var request = $.ajax({
				url: "/login",
				method: "POST",
				dataType : "json",
				data : {username : username, password : password}
			});
			request.done(function(result) {
				if(result.status){
					$('#result').html('Connexion réussie.<br /> <b>(Un cookie a été déposé)</b>');
				}
				else{
					$('#result').html('Mauvais identifiants.');
				}

			});
			request.fail(function(jqXHR, textStatus) {
				$('#result').html('Impossible de se connecter au serveur.<br />Erreur : ' + textStatus);
			});
		}
		else{
			$('#result').html('Vous devez entrer un login et un mot de passe !');
		}
	});
});