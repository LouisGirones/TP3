// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var crypto = require('crypto'); // To create unique token
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

//Sessions 
app.get("/sessions", function(req, res, next) {
	db.all('SELECT rowid, ident, token FROM sessions;', function(err, data) {
		res.json(data);
	});
});

app.get("/cleanSessions", function(req, res, next){
	db.run('DELETE FROM sessions', function(){
		res.json(this);
	})
});

app.post("/login", function(req, res, next) {
	var status = false;
	var token = null;
	var user = {};
	user.username = req.body.username;
	user.password = req.body.password;
	db.get('SELECT COUNT(*) as nbUsers FROM users WHERE ident=? AND password=?;', [user.username, user.password], function(err, data) {
		status = data.nbUsers == 1 ? true : false;
		if (status) {
			token = crypto.randomBytes(64).toString('hex');
			db.run('INSERT INTO sessions (token) VALUES (?)', [token]);
			//Succesfull authentification, sending cookie
			res.cookie('session', { status : status, token : token});
		}
		//Authentification failed
		res.json({status : status, token : token});
	});

});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
	console.log('La liste des sessions est accessible sur http://localhost:' + port + "/sessions (debug)");
	console.log('Il est possible de vider la table sessions sur http://localhost:' + port + "/cleanSessions (debug)");
});
